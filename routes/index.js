const express = require("express");
const router = express.Router();
const partnerPortal = require("../controllers/partner-portal.controller");
const studentPortal = require("../controllers/student-portal.controller");

module.exports.setRouter = (app) => {
  // Defining Routes

  app.get("/hello", partnerPortal.helloWorldFunc);

  //partner portal routes

  app.get("/api/partner", partnerPortal.partner);
  app.get("/api/degree-courses", partnerPortal.degreeCourses);
  app.get("/api/enrollments", partnerPortal.enrollements);
  app.get("/api/partner-portal/grade-report", partnerPortal.gradeReport);
  app.get("/api/grades", partnerPortal.grades);
  app.get("/api/dashboard", partnerPortal.dashboard);
  app.get("/api/partner-portal", partnerPortal.partnerPortal);
  app.get("/api/partner-portal/terms", partnerPortal.terms);
  app.get("/api/learner", partnerPortal.learner);
  app.get("/api/partner-portal/learners", partnerPortal.partnerLearners);
  app.get("/api/partner-portal/zipcode", partnerPortal.partnerPortalZipcode);

  app.post("/api/partner-portal/learners", partnerPortal.postLearners);

  //Stuent portal routes

  app.get("/api/partner-degrees", studentPortal.partnerDegrees);
  app.get("/api/courses", studentPortal.courses);
  app.post("/api/learner", studentPortal.postLearner);
  app.post("/api/enrollments", studentPortal.postEnrollments);
  app.post(
    "/api/ospp_api/v0/course_enrollments",
    studentPortal.postCourseEnrollments
  );
};
